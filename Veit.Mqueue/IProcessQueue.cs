﻿using System;

namespace Veit.Mqueue
{
    public interface IProcessQueue : IQueue
    {
        /// <summary>
        /// Add processing action according to which to process data from the queue.
        /// </summary>
        /// <typeparam name="T">generic type of data this action can process</typeparam>
        /// <param name="action">function which returns true/false depending on whether the data has been processed or not</param>
        /// <returns></returns>
        bool AddProcessing<T>(Func<T, bool> action);
        bool AddProcessing<T>(Type type, Func<T, bool> action);

        /// <summary>
        /// Remove previously registered processing actions of the specific type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        bool RemoveProcessing(Type type);
        bool RemoveProcessing<T>();

        bool RemoveProcessing<T>(Func<T, bool> action);
        bool RemoveProcessing<T>(Type type, Func<T, bool> action);


        /// <summary>
        /// Value in milliseconds for waiting between operations. If set 0, no delay.
        /// </summary>
        int DelayBetweenOperations { get; set; }
    }
}
