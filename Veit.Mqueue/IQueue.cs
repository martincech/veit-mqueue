﻿using System.Collections.Generic;

namespace Veit.Mqueue
{
    public interface IQueue
    {
        /// <summary>
        /// Adds an object to the Queue for further processing.
        /// Object is ordered by priority parameter <see cref="Priority"/>.
        /// If item is PriorityObject <see cref="PriorityObject"/> priority
        /// settings is taken from it and parameter is ignored.
        /// </summary>
        /// <param name="item">object to be added</param>
        /// <returns>true if the object has been added, false otherwise</returns>
        bool Enqueue(object item, Priority priority = Priority.Normal);

        /// <summary>
        /// Adds multiple objects to the Queue for further processing.
        /// Elements are included with common priority by parameter <see cref="Priority"/>.
        /// If elements are <see cref="PriorityObject"/>, this priority is used.
        /// </summary>
        /// <param name="elements"></param>
        /// <param name="priority"></param>
        /// <returns></returns>
        bool EnqueueRange(IEnumerable<object> elements, Priority priority = Priority.Normal);

        /// <summary>
        /// Queue of the added objects
        /// </summary>
        IEnumerable<object> Queue();
    }
}
