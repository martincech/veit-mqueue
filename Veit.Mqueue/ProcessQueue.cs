﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Veit.Mqueue.DataStructures;

namespace Veit.Mqueue
{
    public class ProcessQueue : IProcessQueue, IDisposable
    {
        #region Private fields

        private CancellationTokenSource cts;

        private readonly ConcurrentDictionary<Type, List<Func<object, bool>>> actionsToExecuteForType;
        private readonly ConcurrentDictionary<object, Func<object, bool>> wrappedActions;
        private ConcurrentPriorityCollection<PriorityObject> collection;
        private readonly TaskScheduler scheduler;
        private const int DefaultNoOperationDelay = 50; //milliseconds
        private static readonly object lockObj = new object();

        #endregion

        #region Constructors

        public ProcessQueue()
           : this(new Dictionary<Type, List<Func<object, bool>>>())
        {
        }

        public ProcessQueue(Dictionary<Type, List<Func<object, bool>>> actions)
           : this(actions, TaskScheduler.Default)
        {
        }

        public ProcessQueue(Dictionary<Type, List<Func<object, bool>>> actions, TaskScheduler scheduler)
        {
            actionsToExecuteForType = new ConcurrentDictionary<Type, List<Func<object, bool>>>();
            wrappedActions = new ConcurrentDictionary<object, Func<object, bool>>();
            if (actions != null)
            {
                foreach (var action in actions)
                {
                    if (action.Value == null)
                    {
                        throw new ArgumentException($"Action is not validly registered for type {action.Key}");
                    }

                    foreach (var func in action.Value)
                    {
                        AddProcessing(action.Key, func);
                    }
                }
            }

            this.scheduler = scheduler;
            collection = new ConcurrentPriorityCollection<PriorityObject>();
            DelayBetweenOperations = DefaultNoOperationDelay;
            InitDigest();
        }

        #endregion

        public int DelayBetweenOperations { get; set; }

        public IEnumerable<object> Queue()
        {
            return collection.Select(s => s.Element).ToList();
        }

        public bool Enqueue(object item, Priority priority = Priority.Normal)
        {
            if (item == null)
            {
                return false;
            }

            var element = item;
            PriorityObject insertObject = null;
            if (item is PriorityObject po)
            {
                insertObject = po;
                element = po.Element;
            }
            else
            {
                insertObject = new PriorityObject(item, priority);
            }

            var servableType = actionsToExecuteForType.Keys.FirstOrDefault(key => key.IsInstanceOfType(element));
            if (servableType == null)
            {
                return false;
            }
            if (actionsToExecuteForType[servableType].Any())
            {
                return collection.TryAdd(insertObject);
            }
            return true;
        }

        public bool EnqueueRange(IEnumerable<object> elements, Priority priority = Priority.Normal)
        {
            if (elements == null)
            {
                return true;
            }
            foreach (var element in elements)
            {
                Enqueue(element, priority);
            }
            return true;
        }

        public bool AddProcessing<T>(Func<T, bool> action)
        {
            return AddProcessing(typeof(T), action);
        }

        public bool AddProcessing<T>(Type type, Func<T, bool> action)
        {
            lock (lockObj)
            {
                if (action == null || type == null || wrappedActions.ContainsKey(action))
                {
                    return false;
                }
                if (!actionsToExecuteForType.ContainsKey(type))
                {
                    actionsToExecuteForType.TryAdd(type, new List<Func<object, bool>>());
                }
                actionsToExecuteForType[type].Add(WrapAction(action));
                return true;
            }
        }

        public void RemoveObjectsOfTypeFromQueue<T>()
        {
            Dispose();
            var otherTypes = collection.Where(i => !(i is T)).ToList();

            //other objects except removing type will be return to collection
            collection = new ConcurrentPriorityCollection<PriorityObject>();
            foreach (var item in otherTypes)
            {
                collection.TryAdd(item);
            }
            InitDigest();
        }

        private void InitDigest()
        {
            cts = new CancellationTokenSource();
            Task.Factory.StartNew(StartDigest, cts.Token, TaskCreationOptions.LongRunning, scheduler);
        }


        public bool RemoveProcessing<T>()
        {
            return RemoveProcessing(typeof(T));
        }

        public bool RemoveProcessing(Type type)
        {
            return RemoveRegisteredAction(type);
        }

        private bool RemoveRegisteredAction(Type type, IEnumerable<Func<object, bool>> func = null)
        {
            lock (lockObj)
            {
                if (type == null || !actionsToExecuteForType.ContainsKey(type))
                {
                    return false;
                }
                if (func == null)
                {
                    func = new List<Func<object, bool>>(actionsToExecuteForType[type]);
                }
                foreach (var wrapperFunc in func)
                {
                    wrappedActions.TryRemove(wrappedActions.First(k => k.Value == wrapperFunc).Key, out _);
                    actionsToExecuteForType[type].Remove(wrapperFunc);
                }
                return true;
            }
        }


        public bool RemoveProcessing<T>(Func<T, bool> action)
        {
            return RemoveProcessing(typeof(T), action);
        }

        public bool RemoveProcessing<T>(Type type, Func<T, bool> action)
        {
            if (!wrappedActions.ContainsKey(action))
            {
                return false;
            }
            var wrapperForAction = wrappedActions[action];
            return RemoveRegisteredAction(type, new List<Func<object, bool>> { wrapperForAction });
        }

        private Func<object, bool> WrapAction<T>(Func<T, bool> action)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            var wrapper = new Func<object, bool>(a =>
            {
                var item = (T)a;
                return item != null && action(item);
            });
            wrappedActions.TryAdd(action, wrapper);
            return wrapper;
        }

        private void StartDigest()
        {
            while (!cts.IsCancellationRequested)
            {
                if (!collection.TryTake(out var priorityObject))
                {
                    DelayTask(cts.Token);
                    continue;
                }

                Digest(priorityObject.Element);
                if (DelayBetweenOperations > 0)
                {
                    DelayTask(cts.Token, DelayBetweenOperations);
                }
            }
        }

        private static void DelayTask(CancellationToken cancel, int delay = DefaultNoOperationDelay)
        {
            Task.Delay(delay, cancel).GetAwaiter().GetResult();
        }

        private void Digest(object element)
        {
            lock (lockObj)
            {
                if (element == null)
                {
                    return;
                }
                if (!actionsToExecuteForType.Keys.Any(key => key.IsInstanceOfType(element)))
                {
                    return;
                }
                foreach (var action in actionsToExecuteForType[
                    actionsToExecuteForType.Keys.First(key => key.IsInstanceOfType(element))].ToArray())
                {
                    action(element);
                }
            }
        }

        #region Implementation of IDisposable

        protected virtual void Dispose(bool disposing)
        {
            if (cts == null) return;
            cts.Cancel();
            cts.Dispose();
            cts = null;
        }

        /// <summary>
        ///    Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
