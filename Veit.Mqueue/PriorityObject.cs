﻿using System;
using System.Collections.Generic;

namespace Veit.Mqueue
{
    public class PriorityObject : Comparer<PriorityObject>, IComparable, IEquatable<PriorityObject>
    {
        private readonly DateTime createTimeStamp;

        public PriorityObject(object element, Priority priority = Priority.Low)
        {
            Element = element;
            Priority = priority;
            createTimeStamp = DateTime.Now;
        }

        public object Element { get; }
        public Priority Priority { get; }


        public override int Compare(PriorityObject x, PriorityObject y)
        {
            var priorityCmp = x.Priority.CompareTo(y.Priority);
            if (priorityCmp != 0)
            {
                return priorityCmp;
            }

            if (x.Priority == Priority.High)
            {   //LIFO behavior
                return x.createTimeStamp.CompareTo(y.createTimeStamp);
            }

            //FIFO behavior in concrete priority level
            return y.createTimeStamp.CompareTo(x.createTimeStamp);
        }

        public int CompareTo(object obj)
        {
            if (!(obj is PriorityObject po))
            {
                throw new ArgumentException($"Parameter: {obj.GetType()} is not correct type, expected is: {GetType()}");
            }
            return Compare(this, po);
        }

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((PriorityObject)obj);
        }

        public virtual bool Equals(PriorityObject other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return
                (
                    Element == other.Element ||
                    Element != null &&
                    Element.Equals(other.Element)
                ) &&
                (
                    Priority == other.Priority ||
                    Priority.Equals(other.Priority)
                );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 41;
                if (Element != null)
                {
                    hashCode = hashCode * 59 + Element.GetHashCode();
                }
                hashCode = hashCode * 59 + Priority.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(PriorityObject left, PriorityObject right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PriorityObject left, PriorityObject right)
        {
            return !Equals(left, right);
        }

        public static bool operator <(PriorityObject left, PriorityObject right)
        {
            return left == null ? right != null : left.CompareTo(right) < 0;
        }

        public static bool operator <=(PriorityObject left, PriorityObject right)
        {
            return left == null || left.CompareTo(right) <= 0;
        }

        public static bool operator >(PriorityObject left, PriorityObject right)
        {
            return left != null && left.CompareTo(right) > 0;
        }

        public static bool operator >=(PriorityObject left, PriorityObject right)
        {
            return left == null ? right == null : left.CompareTo(right) >= 0;
        }
    }
}
