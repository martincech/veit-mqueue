﻿/*
 * Taken from: https://github.com/sphinxy/DataStructures
 * License: MIT
 * Copyright (c) 2015 Denis Shulepov
 */
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

namespace Veit.Mqueue.DataStructures
{
    /// <summary>
    /// Thread-safe heap-based re-sizable max-priority queue.
    /// Elements with high priority are served before elements with low priority.
    /// Priority is defined by comparing elements, so to separate priority from value use
    /// KeyValuePair or a custom class and provide corresponding Comparer.
    /// </summary>
    /// <typeparam name="T">Any comparable type, either through a specified Comparer or implementing IComparable&lt;<typeparamref name="T"/>&gt;</typeparam>
    public class ConcurrentPriorityCollection<T> : PriorityCollection<T>, IProducerConsumerCollection<T>, IDisposable
    {
        private readonly ReaderWriterLockSlim lockSlim = new ReaderWriterLockSlim();

        /// <summary>
        /// Create concurrent max-priority queue with default capacity of 10.
        /// </summary>
        /// <param name="comparer">Custom comparer to compare elements. If omitted - default will be used.</param>
        public ConcurrentPriorityCollection(IComparer<T> comparer = null) : base(comparer)
        {
        }

        /// <summary>
        /// Create concurrent max-priority queue with provided capacity.
        /// </summary>
        /// <param name="capacity">Initial capacity</param>
        /// <param name="comparer">Custom comparer to compare elements. If omitted - default will be used.</param>
        /// <exception cref="ArgumentOutOfRangeException">Throws <see cref="ArgumentOutOfRangeException"/> when capacity is less than or equal to zero.</exception>
        /// <exception cref="ArgumentException">Throws <see cref="ArgumentException"/> when comparer is null and <typeparamref name="T"/> is not comparable</exception>
        public ConcurrentPriorityCollection(int capacity, IComparer<T> comparer = null) : base(capacity, comparer)
        {
        }

        public override void Add(T item)
        {
            lockSlim.EnterWriteLock();
            try
            {
                base.Add(item);
            }
            finally
            {
                lockSlim.ExitWriteLock();
            }
        }

        public bool TryAdd(T item)
        {
            Add(item);
            return true;
        }

        public override void Clear()
        {
            lockSlim.EnterWriteLock();
            try
            {
                base.Clear();
            }
            finally
            {
                lockSlim.ExitWriteLock();
            }
        }

        public override bool Contains(T item)
        {
            lockSlim.EnterReadLock();
            try
            {
                return base.Contains(item);
            }
            finally
            {
                lockSlim.ExitReadLock();
            }
        }

        public override void CopyTo(T[] array, int arrayIndex)
        {
            var hasLock = lockSlim.IsReadLockHeld;
            if (!hasLock) lockSlim.EnterReadLock();
            try
            {
                base.CopyTo(array, arrayIndex);
            }
            finally
            {
                if (!hasLock) lockSlim.ExitReadLock();
            }
        }

        public override void CopyTo(Array array, int arrayIndex)
        {
            lockSlim.EnterReadLock();
            try
            {
                base.CopyTo((T[])array, arrayIndex);
            }
            finally
            {
                lockSlim.ExitReadLock();
            }
        }

        public T[] ToArray()
        {
            lockSlim.EnterReadLock();
            try
            {
                var array = new T[Count];
                base.CopyTo(array, 0);
                return array;
            }
            finally
            {
                lockSlim.ExitReadLock();
            }
        }

        public override IEnumerator<T> GetEnumerator()
        {
            lockSlim.EnterReadLock();
            try
            {
                return base.GetEnumerator();
            }
            finally
            {
                lockSlim.ExitReadLock();
            }
        }

        public override T Peek()
        {
            lockSlim.EnterReadLock();
            try
            {
                return base.Peek();
            }
            finally
            {
                lockSlim.ExitReadLock();
            }
        }

        public override bool Remove(T item)
        {
            lockSlim.EnterUpgradeableReadLock();
            try
            {
                var index = GetItemIndex(item);
                switch (index)
                {
                    case -1:
                        return false;
                    case 0:
                        // Take does locking on it's own
                        Take();
                        break;
                    default:
                        lockSlim.EnterWriteLock();
                        try
                        {
                            // provide a 1-based index of the item
                            RemoveAt(index + 1, shift: -1);
                        }
                        finally
                        {
                            lockSlim.ExitWriteLock();
                        }
                        break;
                }
            }
            finally
            {
                lockSlim.ExitUpgradeableReadLock();
            }
            return true;
        }

        public override T Take()
        {
            lockSlim.EnterWriteLock();
            try
            {
                return base.Take();
            }
            finally
            {
                lockSlim.ExitWriteLock();
            }
        }

        public bool TryTake(out T item)
        {
            item = default(T);
            lockSlim.EnterUpgradeableReadLock();
            try
            {
                if (Count == 0) return false;
                item = Take();
                return true;
            }
            finally
            {
                lockSlim.ExitUpgradeableReadLock();
            }
        }

        public object SyncRoot
        {
            get { throw new NotSupportedException(""); }
        }

        public bool IsSynchronized
        {
            get { return false; }
        }

        protected virtual void Dispose(bool disposing)
        {
            lockSlim.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
