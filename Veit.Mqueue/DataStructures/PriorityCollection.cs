﻿/*
 * Taken from: https://github.com/sphinxy/DataStructures
 * License: MIT
 * Copyright (c) 2015 Denis Shulepov
 */
using System;
using System.Collections;
using System.Collections.Generic;

namespace Veit.Mqueue.DataStructures
{
    /// <summary>
    /// Heap-based re-sizable max-priority queue.
    /// Elements with high priority are served before elements with low priority.
    /// Priority is defined by comparing elements, so to separate priority from value use
    /// KeyValuePair or a custom class and provide corresponding Comparer.
    /// </summary>
    /// <typeparam name="T">Any comparable type, either through a specified Comparer or implementing IComparable&lt;<typeparamref name="T"/>&gt;</typeparam>
    public class PriorityCollection<T> : ICollection<T>
    {
        private readonly IComparer<T> comparer;
        internal T[] heap;
        private const int DefaultCapacity = 10;
        private const int ShrinkRatio = 4;
        private const int ResizeFactor = 2;

        private int shrinkBound;
        private readonly InvalidOperationException emptyCollectionException = new InvalidOperationException("Collection is empty.");

        /// <summary>
        /// Create a max-priority queue with default capacity of 10.
        /// </summary>
        /// <param name="comparer">Custom comparer to compare elements. If omitted - default will be used.</param>
        public PriorityCollection(IComparer<T> comparer = null)
            : this(DefaultCapacity, comparer)
        {
        }

        /// <summary>
        /// Create a max-priority queue with provided capacity.
        /// </summary>
        /// <param name="capacity">Initial capacity</param>
        /// <param name="comparer">Custom comparer to compare elements. If omitted - default will be used.</param>
        /// <exception cref="ArgumentOutOfRangeException">Throws <see cref="ArgumentOutOfRangeException"/> when capacity is less than or equal to zero.</exception>
        /// <exception cref="ArgumentException">Throws <see cref="ArgumentException"/> when comparer is null and <typeparamref name="T"/> does not implement IComparable.</exception>
        public PriorityCollection(int capacity, IComparer<T> comparer = null)
        {
            if (capacity <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity), "Expected capacity greater than zero.");
            }

            // If no comparer then T must be comparable
            if (comparer == null &&
                !(typeof(IComparable).IsAssignableFrom(typeof(T)) ||
                  typeof(IComparable<T>).IsAssignableFrom(typeof(T))))
            {
                throw new ArgumentException("Expected a comparer for types, which do not implement IComparable.", nameof(comparer));
            }

            this.comparer = comparer ?? Comparer<T>.Default;
            shrinkBound = capacity / ShrinkRatio;
            heap = new T[capacity];
        }

        /// <summary>
        /// Current queue capacity
        /// </summary>
        public int Capacity { get { return heap.Length; } }

        public virtual IEnumerator<T> GetEnumerator()
        {
            var array = new T[Count];
            CopyTo(array, 0);
            return ((IEnumerable<T>)array).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public virtual void Add(T item)
        {
            if (Count == Capacity)
            {
                GrowCapacity();
            }

            heap[Count++] = item;
            // provide the index of the last item as for 1-based heap, but also set shift to -1
            heap.Sift(Count, comparer, shift: -1);      // move item "up" until heap principles are not met
        }

        /// <summary>
        /// Removes and returns a max element from the priority queue.
        /// </summary>
        /// <returns>Max element in the collection</returns>
        /// <exception cref="InvalidOperationException">Throws <see cref="InvalidOperationException"/> when queue is empty.</exception>
        public virtual T Take()
        {
            if (Count == 0)
            {
                throw emptyCollectionException;
            }

            var item = heap[0];
            Count--;
            heap.Swap(0, Count);              // last element at count
            heap[Count] = default(T);         // release hold on the object
            // provide index of first item as for 1-based heap, but also set shift to -1
            heap.Sink(1, Count, comparer, shift: -1);   // move item "down" while heap principles are not met

            if (Count <= shrinkBound && Count > DefaultCapacity)
            {
                ShrinkCapacity();
            }

            return item;
        }

        /// <summary>
        /// Returns a max element from the priority queue without removing it.
        /// </summary>
        /// <returns>Max element in the collection</returns>
        /// <exception cref="InvalidOperationException">Throws <see cref="InvalidOperationException"/> when queue is empty.</exception>
        public virtual T Peek()
        {
            if (Count == 0)
            {
                throw emptyCollectionException;
            }
            return heap[0];
        }

        public virtual void Clear()
        {
            heap = new T[DefaultCapacity];
            Count = 0;
        }

        public virtual bool Contains(T item)
        {
            return GetItemIndex(item) >= 0;
        }

        public virtual void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }
            if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(arrayIndex));
            }
            if (array.Length - arrayIndex < Count)
            {
                throw new ArgumentException("Insufficient space in destination array.");
            }

            Array.Copy(heap, 0, array, arrayIndex, Count);
            array.HeapSort(arrayIndex, Count, comparer);
        }

        public virtual void CopyTo(Array array, int arrayIndex)
        {
            CopyTo((T[])array, arrayIndex);
        }

        public virtual bool Remove(T item)
        {
            var index = GetItemIndex(item);
            switch (index)
            {
                case -1:
                    return false;
                case 0:
                    Take();
                    break;
                default:
                    // provide a 1-based index of the item
                    RemoveAt(index + 1, shift: -1);
                    break;
            }

            return true;
        }

        public int Count { get; private set; }

        public bool IsReadOnly { get { return false; } }

        /// <summary>
        /// Removes item at given index
        /// </summary>
        /// <param name="index">1-based index of the element to remove</param>
        /// <param name="shift">Shift allows to compensate and work with arrays where heap starts not from the element at position 1.
        /// Shift -1 allows to work with 0-based heap as if it was 1-based. But the main reason for this is the CopyTo method.</param>
        protected internal void RemoveAt(int index, int shift)
        {
            var itemIndex = index + shift;
            Count--;
            heap.Swap(itemIndex, Count);       // last element at Count
            heap[Count] = default(T);          // release hold on the object
            // use a 1-based-heap index and then apply shift of -1
            var parent = index / 2 + shift;     // get parent
            // if new item at index is greater than it's parent then sift it up, else sink it down
            if (comparer.GreaterOrEqual(heap[itemIndex], heap[parent]))
            {
                // provide a 1-based-heap index
                heap.Sift(index, comparer, shift);
            }
            else
            {
                // provide a 1-based-heap index
                heap.Sink(index, Count, comparer, shift);
            }
        }

        /// <summary>
        /// Returns the real index of the first occurrence of the given item or -1.
        /// </summary>
        /// <param name="item"></param>
        protected internal int GetItemIndex(T item)
        {
            for (var i = 0; i < Count; i++)
            {
                if (comparer.Compare(heap[i], item) == 0) return i;
            }
            return -1;
        }


        private void GrowCapacity()
        {
            var newCapacity = Capacity * ResizeFactor;
            Array.Resize(ref heap, newCapacity);  // first element is at position 1
            shrinkBound = newCapacity / ShrinkRatio;
        }

        private void ShrinkCapacity()
        {
            var newCapacity = Capacity / ResizeFactor;
            Array.Resize(ref heap, newCapacity);  // first element is at position 1
            shrinkBound = newCapacity / ShrinkRatio;
        }
    }
}
