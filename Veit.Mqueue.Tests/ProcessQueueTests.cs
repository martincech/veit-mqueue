﻿using System;
using System.Collections.Generic;
using System.Threading;
using Veit.Mqueue.DataStructures;
using Xunit;

namespace Veit.Mqueue.Tests
{
    public sealed class ProcessQueueTests : IDisposable
    {
        private ProcessQueue UnitUnderTest;
        private readonly DeterministicTaskScheduler scheduler;
        private readonly ConcurrentPriorityCollection<PriorityObject> privateCollection;

        public ProcessQueueTests()
        {
            scheduler = new DeterministicTaskScheduler();
            UnitUnderTest = new ProcessQueue(null, scheduler)
            {
                DelayBetweenOperations = 0
            };
            privateCollection = UnitUnderTest.GetType()
                .GetField("collection", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(UnitUnderTest) as ConcurrentPriorityCollection<PriorityObject>;
            Assert.NotNull(privateCollection);
        }


        private void DoAsyncProcessing()
        {
            ThreadPool.QueueUserWorkItem(o =>
            {
                while (privateCollection.Count > 0)
                {
                }
                UnitUnderTest.Dispose();
            });

            scheduler.RunPendingTasks();
        }

        [Fact]
        public void Action_InvokedAfterRegistration()
        {
            const int value = 5;
            var raised = false;
            UnitUnderTest.AddProcessing<int>(i =>
            {
                raised = true;
                Assert.Equal(value, i);
                return true;
            });
            UnitUnderTest.Enqueue(value);
            DoAsyncProcessing();
            Assert.True(raised);
        }


        [Fact]
        public void Process_Asynchronously()
        {
            const int value = 5;
            var raised = false;
            UnitUnderTest.AddProcessing<int>(i =>
            {
                raised = true;
                Assert.Equal(value, i);
                return true;
            });
            UnitUnderTest.Enqueue(value);
            Assert.False(raised);
            DoAsyncProcessing();
            Assert.True(raised);
        }

        [Fact]
        public void Process_MultipleInvocationWithTheSameData()
        {
            const int value = 5;
            var raisedTimes = 0;
            UnitUnderTest.AddProcessing<int>(i =>
            {
                raisedTimes++;
                Assert.Equal(value, i);
                return true;
            });
            for (var i = 0; i < value; i++)
            {
                UnitUnderTest.Enqueue(value);
            }

            DoAsyncProcessing();
            Assert.Equal(value, raisedTimes);
        }

        [Fact]
        public void Proces_MultipleActions()
        {
            const int value = 5;
            var raisedFirst = false;
            var raisedSecond = false;
            UnitUnderTest.AddProcessing<int>(i =>
            {
                raisedFirst = true;
                return true;
            });
            UnitUnderTest.AddProcessing<int>(i =>
            {
                raisedSecond = true;
                return true;
            });
            UnitUnderTest.Enqueue(value);
            DoAsyncProcessing();
            Assert.True(raisedFirst);
            Assert.True(raisedSecond);
        }

        [Fact]
        public void Process_DifferentDataTypes()
        {
            var intProcessed = false;
            var doubleProcessed = false;
            var stringProcessed = false;
            var uutProcessed = false;
            var values = new object[]
            {
                5,
                5.3,
                "string",
                UnitUnderTest
            };

            UnitUnderTest.AddProcessing<int>(i =>
            {
                intProcessed = true;
                Assert.Equal(values[0], i);
                return true;
            });
            UnitUnderTest.AddProcessing<double>(i =>
            {
                doubleProcessed = true;
                Assert.Equal(values[1], i);
                return true;
            });
            UnitUnderTest.AddProcessing<string>(i =>
            {
                stringProcessed = true;
                Assert.Equal(values[2], i);
                return true;
            });
            UnitUnderTest.AddProcessing<ProcessQueue>(i =>
            {
                uutProcessed = true;
                Assert.Equal(values[3], i);
                return true;

            });


            UnitUnderTest.EnqueueRange(values);
            DoAsyncProcessing();

            Assert.True(intProcessed);
            Assert.True(doubleProcessed);
            Assert.True(stringProcessed);
            Assert.True(uutProcessed);
        }

        [Fact]
        public void NullActionCantRegister()
        {
            Assert.False(UnitUnderTest.AddProcessing<int>(null));
            Assert.False(UnitUnderTest.AddProcessing<double>(typeof(int), null));
        }

        [Fact]
        public void SameAction_CanBeRegisteredOnlyOnce()
        {
            var action = new Func<int, bool>(i => true);
            Assert.True(UnitUnderTest.AddProcessing(action));
            Assert.False(UnitUnderTest.AddProcessing(action));
        }


        [Fact]
        public void DisposeStopsProcessing()
        {
            var intProcessed = false;
            UnitUnderTest.AddProcessing<int>(i =>
            {
                intProcessed = true;
                return true;
            });
            UnitUnderTest.Enqueue(5);
            using (UnitUnderTest)
            {
            }
            scheduler.RunPendingTasks();
            Assert.False(intProcessed);

        }

        [Fact]
        public void InvalidConstructorCall_ThrowArgumentException_ForInvalidAction()
        {
            var exception = Record.Exception(() =>
            {
                UnitUnderTest = new ProcessQueue(new Dictionary<Type, List<Func<object, bool>>>
                 {
                    {typeof (int), null}
                 });
            });
            Assert.NotNull(exception);
            Assert.IsType<ArgumentException>(exception);
        }

        [Fact]
        public void ActionTypeDeregistered()
        {
            var intProcessed = false;
            var action = new Func<int, bool>(i =>
            {
                intProcessed = true;
                return true;
            });
            UnitUnderTest.AddProcessing(action);
            UnitUnderTest.RemoveProcessing(typeof(int));
            DoAsyncProcessing();
            Assert.False(intProcessed);
        }

        [Fact]
        public void ActionTypeDeregistered_ForMultipleRegistrations()
        {
            var intProcessed = false;
            var action = new Func<int, bool>(i =>
            {
                intProcessed = true;
                return true;
            });
            var action2 = new Func<int, bool>(i =>
            {
                intProcessed = true;
                return false;
            });
            UnitUnderTest.AddProcessing(action);
            UnitUnderTest.AddProcessing(action2);
            UnitUnderTest.RemoveProcessing<int>();
            DoAsyncProcessing();
            Assert.False(intProcessed);
        }

        [Fact]
        public void ActionDeregistered()
        {
            const int value = 5;
            var raisedFirst = false;
            var raisedSecond = false;

            bool action1(int i)
            {
                raisedFirst = true;
                return true;
            }
            bool action2(int i)
            {
                raisedSecond = true;
                return true;
            }

            UnitUnderTest.AddProcessing<int>(action1);
            UnitUnderTest.AddProcessing<int>(action2);
            UnitUnderTest.Enqueue(value);
            UnitUnderTest.RemoveProcessing<int>(action2);
            DoAsyncProcessing();
            Assert.True(raisedFirst);
            Assert.False(raisedSecond);
        }

        [Fact]
        public void QueueContainsMyItem()
        {
            const int o = 5;
            var action = new Func<int, bool>(i => true);
            UnitUnderTest.AddProcessing(action);
            UnitUnderTest.Enqueue(o);
            Assert.Contains(o, UnitUnderTest.Queue());
        }

        [Fact]
        public void QueueDoesNotEnqueueItem_WhichDoesNotHaveProcessing()
        {
            const int intValue = 5;
            const double doubleValue = 1.5;
            var action = new Func<int, bool>(i => true);

            UnitUnderTest.AddProcessing(action);
            Assert.True(UnitUnderTest.Enqueue(intValue));
            Assert.False(UnitUnderTest.Enqueue(doubleValue));
            Assert.Contains(intValue, UnitUnderTest.Queue());
            Assert.DoesNotContain(doubleValue, UnitUnderTest.Queue());
        }

        [Fact]
        public void QueueDoesEnqueueItem_Which_DoesEverHadProcessing()
        {
            const int intValue = 5;
            var action = new Func<int, bool>(i => true);

            UnitUnderTest.AddProcessing(action);
            UnitUnderTest.RemoveProcessing(action);
            Assert.True(UnitUnderTest.Enqueue(intValue));
            Assert.DoesNotContain(intValue, UnitUnderTest.Queue());
        }

        [Fact]
        public void PriorityOrderInQueue()
        {
            // Arrange
            var output = new List<int>();
            var raisedCount = 0;
            bool action(int value)
            {
                output.Add(value);
                raisedCount++;
                Thread.Sleep(20);
                return true;
            }
            UnitUnderTest.AddProcessing<int>(action);

            // Act
            UnitUnderTest.Enqueue(1, Priority.Normal);
            UnitUnderTest.Enqueue(2, Priority.Normal);
            UnitUnderTest.Enqueue(3, Priority.Normal);
            UnitUnderTest.Enqueue(4, Priority.High);
            UnitUnderTest.Enqueue(5, Priority.High);
            UnitUnderTest.Enqueue(6, Priority.Low);
            UnitUnderTest.Enqueue(7, Priority.Low);
            UnitUnderTest.Enqueue(8, Priority.Normal);
            DoAsyncProcessing();

            // Assert
            Assert.Equal(5, output[0]);
            Assert.Equal(4, output[1]);
            Assert.Equal(1, output[2]);
            Assert.Equal(2, output[3]);
            Assert.Equal(3, output[4]);
            Assert.Equal(8, output[5]);
            Assert.Equal(6, output[6]);
            Assert.Equal(7, output[7]);
        }

        public void Dispose()
        {
            UnitUnderTest?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
