﻿using Veit.Mqueue.DataStructures;
using Xunit;

namespace Veit.Mqueue.Tests.DataStructures
{
    public class ConcurrentPriorityCollectionTests
    {
        [Fact]
        public void PriorityTest()
        {
            using (var queue = new ConcurrentPriorityCollection<PriorityObject>())
            {
                // Arrange
                queue.TryAdd(new PriorityObject(1, Priority.Low));
                queue.TryAdd(new PriorityObject(2, Priority.Low));
                queue.TryAdd(new PriorityObject(3, Priority.High));
                queue.TryAdd(new PriorityObject(4, Priority.Normal));
                queue.TryAdd(new PriorityObject(5, Priority.Low));
                queue.TryAdd(new PriorityObject(6, Priority.High));
                queue.TryAdd(new PriorityObject(7, Priority.Normal));
                queue.TryAdd(new PriorityObject(8, Priority.Normal));
                queue.TryAdd(new PriorityObject(9, Priority.Low));

                //Act
                var priorityOrderedList = queue.ToArray();

                // Assert
                Assert.Equal(6, priorityOrderedList[0].Element);
                Assert.Equal(3, priorityOrderedList[1].Element);
                Assert.Equal(4, priorityOrderedList[2].Element);
                Assert.Equal(7, priorityOrderedList[3].Element);
                Assert.Equal(8, priorityOrderedList[4].Element);
                Assert.Equal(1, priorityOrderedList[5].Element);
                Assert.Equal(2, priorityOrderedList[6].Element);
                Assert.Equal(5, priorityOrderedList[7].Element);
                Assert.Equal(9, priorityOrderedList[8].Element);
            }
        }
    }
}
