﻿using System.Collections.Generic;
using Xunit;

namespace Veit.Mqueue.Tests
{
    public class PriorityObjectTests
    {
        private readonly Comparer<PriorityObject> defaultComparer;

        public PriorityObjectTests()
        {
            defaultComparer = PriorityObject.Default;
        }


        #region Default comparer tests

        [Theory]
        [MemberData(nameof(GetPrioritiesObjects))]
        public void CompareSamePrioritySameCreateTime(PriorityObject priorityObj)
        {
            // Arrange
            // member data

            //Act
            var compareResult = defaultComparer.Compare(priorityObj, priorityObj);

            // Assert
            Assert.Equal(0, compareResult);
        }

        public static IEnumerable<object[]> GetPrioritiesObjects
        {
            get
            {
                return new[]
                {
                    new object[] { null },
                    new object[] { new PriorityObject(null, Priority.Low) },
                    new object[] { new PriorityObject(null, Priority.Normal) },
                    new object[] { new PriorityObject(null, Priority.High) }
                };
            }
        }

        [Theory]
        [MemberData(nameof(GetObjectsXAreLowerThanY))]
        public void CompareXIsLowerThanY(PriorityObject x, PriorityObject y)
        {
            // Arrange
            // member data

            //Act
            var compareResult = defaultComparer.Compare(x, y);

            // Assert
            Assert.True(compareResult < 0);
        }

        [Theory]
        [MemberData(nameof(GetObjectsXAreLowerThanY))]
        public void CompareXIsHigherThanY(PriorityObject x, PriorityObject y)
        {
            // Arrange
            // member data

            //Act
            var compareResult = defaultComparer.Compare(y, x);

            // Assert
            Assert.True(compareResult > 0);
        }

        public static IEnumerable<object[]> GetObjectsXAreLowerThanY
        {
            get
            {
                return new[]
                {
                    new object[] { null, new PriorityObject(null) },
                    new object[] { new PriorityObject(null, Priority.Normal), new PriorityObject(null, Priority.High) },
                    new object[] { new PriorityObject(null, Priority.Low), new PriorityObject(null, Priority.High) },
                    new object[] { new PriorityObject(null, Priority.Low), new PriorityObject(null, Priority.Normal) },
                    new object[] { new PriorityObject(null, Priority.High), new PriorityObject(null, Priority.High) },  //same priorities, but different creation time, second object has higher time stamp (newer date) => second object is greater
                };
            }
        }


        [Theory]
        [MemberData(nameof(GetObjectsWithSamePriorities))]
        public void CompareSamePrioritiesNotHighWithDifferentCreationTime(PriorityObject x, PriorityObject y)
        {
            // Arrange
            // member data

            //Act
            var compareResult = defaultComparer.Compare(x, y);

            // Assert
            Assert.True(compareResult > 0);
        }

        public static IEnumerable<object[]> GetObjectsWithSamePriorities
        {
            get
            {
                return new[]
                {   //same priorities, different creation time, second object is newer, but it has lower priority
                    new object[] { new PriorityObject(null, Priority.Low), new PriorityObject(null, Priority.Low) },
                    new object[] { new PriorityObject(null, Priority.Normal), new PriorityObject(null, Priority.Normal) }
                };
            }
        }

        #endregion

    }
}
